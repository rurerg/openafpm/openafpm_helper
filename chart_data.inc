<?php
function _ntua_wind_service_access($op, $args = array()){
  return true;
}

function _ntua_wind_series_data_t_d($data) {
  $xAxis = range(1,$fata['iteration_max']);
  for ($n = 1; $n < $data['n_particle']; $n++) {
    $series[] = array(
      'data' => $data['d'][$n],
      'color' => '#058DC7',
      'showInLegend' => ($n == 1),
      'title' => 'la',
      'showPoints' => false,
      'ttFormat' => "{point.x:.2f} <br>{point.y:.2f}",
    );
  }
  for ($n = $data['n_particle']; $n <= $data['n_particle']*2-1; $n++) {
    $series[] = array(
      'data' => $data['d'][$n],
      'color' => '#ED561B',
      'showInLegend' => ($n == $data['n_particle']),
      'title' => 'wm',
      'showPoints' => false,
      'ttFormat' => "{point.x:.2f} <br>{point.y:.2f}",

    );
  }
  for ($n = $data['n_particle']*2; $n <= $data['n_particle']*3-1; $n++) {
    $series[] = array(
      'data' => $data['d'][$n],
      'color' => '#50B432',
      'showInLegend' => ($n == $data['n_particle']*2),
      'title' => 'hm',
      'showPoints' => false,
      'ttFormat' => "{point.x:.2f} <br>{point.y:.2f}",

    );
  }
  for ($n = $data['n_particle']*3; $n <= $data['n_particle']*4-1; $n++) {
    $series[] = array(
      'data' => $data['d'][$n],
      'color' => '#FFF263',
      'showInLegend' => ($n == $data['n_particle']*3),
      'title' => 'tw',
      'showPoints' => false,
      'ttFormat' => "{point.x:.2f} <br>{point.y:.2f}",
    );
  }
  return $series;
}

function _ntua_wind_series_data_t_S($data) {
  if ($data['Eff_weigth']==0 && $data['Mass_weight']==0)
     $suffix = '€';
  elseif ($data['Eff_weigth']==0 && $data['Cost_weight']==0)
    $suffix = 'kg';
  foreach ($data['S'] as $key => $values) {
    $series[] = array(
      'data' => $data['S'][$key],
      'title' => 'Particle '.($key+1),
      'ttFormat' => "Iteration {point.x}<br>Value: {point.y:.2f} ".$suffix,
    );
  }
  return $series;
}

function _ntua_wind_series_data_L_Satur_B_Satur ($data) {
  // dpm($data['L_Satur']);
  // dpm($data['B_Satur']);
  $series_data = array();
  foreach ($data['L_Satur'] as $key => $value) {
    $series_data[] = array(
      $value[0],
      $data['B_Satur'][$key][0],
    );
  }
  $series[] = array(
    'data' => $series_data,
    'showInLegend' => false,
    'ttFormat' => "{point.x:.2f} mm<br>{point.y:.2f} T".$suffix,
  );
  // dpm($series);
  return $series;
}

function _ntua_wind_yaxis_r_S($data) {
  if ($data['Cost_weight']==0 && $data['Mass_weight']==0)
     return('Generator Efficiency');
  elseif ($data['Eff_weigth']==0 && $data['Mass_weight']==0)
     return('Generator Cost (EUR)');
  elseif ($data['Eff_weigth']==0 && $data['Cost_weight']==0)
     return('Generator Mass (kg)');
  else
      return('Combined output');
}

/*
* Generate chart data
*/
function _ntua_wind_get_chart_data($nid, $chart_type) {
  require_once libraries_get_path('spyc') . '/spyc.php';

  $axis = explode(',', $chart_type);
  $node = node_load($nid);
  @$result_data = drupal_json_decode($node->field_json_result[LANGUAGE_NONE][0]['value']);
  // YAML defined charts
  $charts = Spyc::YAMLLoad(drupal_get_path('module', 'ntua_wind') . '/yaml/charts.yaml');
  if (array_key_exists($chart_type, $charts)) {
    $xAxis = $axis[0];
    $yAxis = $axis[1];
    $response = $charts[$chart_type];
    // populate serieskey
    if (isset($response['series_callback'])) {
      $response['series'] = $response['series_callback']($result_data);
      unset($response['series_callback']);
    } else {
      $response['series'] = array();
    }
    // Set Y Axis titile
    if (isset($response['yAxis']['callback'])) {
      $response['yAxis']['title'] = $response['yAxis']['callback']($result_data);
      unset($response['yAxis']['callback']);
    } else {
      $response['yAxis']['title'] = ntua_wind_chart_axis_title($yAxis);
    }
    // Set X Axis titile
    if (isset($response['xAxis']['callback'])) {
      $response['xAxis']['title'] = $response['xAxis']['callback']($result_data);
      unset($response['xAxis']['callback']);
    } else {
      $response['xAxis']['title'] = ntua_wind_chart_axis_title($xAxis);
    }
    // foreach ($response['series_vars'] as $varname) {
    //   $response['series'][] = array(
    //     'title' => $varname,
    //     'data' => array($result_data[$varname]),
    //     // 'axis' => $response['series_vars'],
    //   );
    // }
    // dpm($response);
    return $response;
  }

  $axis = explode(',', $chart_type);
  $series = array();
  $seriesTitle = 'Series ';
  // remove series name for charts
  $noSName = array(
    'time,angledeg',
    'Irms,PcopperN',
    'n,ProtN',
    'Irms,Torque_x_avg',
    'n,PeddyN',
    'n,Trot',
    'Irms,FluxRms',
    'Bmaxfile',
    'Bmgfile',
    'n,freqN',
    'Irms,deltaNdeg'
  );
  // To axies request
  if (count($axis) == 2) {
    $xAxis = $axis[0];
    $yAxis = $axis[1];


    if ($xAxis == 'null') {
      $result_data['null'][0] = range(1, count($result_data[$yAxis][0]));
    }

    // dpm(count($result_data[$xAxis][0]),$xAxis);
    // dpm(count($result_data[$yAxis][0]),$yAxis);

    // transpose id needed
    // if (count($result_data[$xAxis][0]) == count($result_data[$yAxis][0])) {

    if ($xAxis == 'Irms' &&
      count($result_data[$xAxis][0]) != count($result_data[$yAxis][0]) &&
      count($result_data[$yAxis]) > 1) {
      // $yAxisData=$result_data[$yAxis];
      foreach ($result_data[$yAxis] as $rowKey => $row) {
        foreach ($row as $colKey => $value) {
          $yAxisData[$colKey][$rowKey] = $value;
        }
      }
    } elseif ($chart_type == 'L_Satur,B_Satur') {
          // dpm($result_data);
    } else {
      $yAxisData=$result_data[$yAxis];
    }
    // dpm($result_data[$xAxis]);
    foreach ($yAxisData as $serieskey => $seriesvalues) {
      $chart_data = array();
      foreach ($seriesvalues as $key => $value) {
        if ( (is_numeric($result_data[$xAxis][0][$key])) && (is_numeric($value))) {
          // drupal_set_message($result_data[$xAxis][0][$key].' => '.$value);
          $chart_data[] = array($result_data[$xAxis][0][$key],$value);
        }
      }

      switch ($xAxis) {
        case 'n':
        case 'time':
        case 'angledeg':
          $seriesTitle = 'Irms';
          $seriesTitleValue = number_format($result_data[$seriesTitle][0][$serieskey],1);
          break;
        case 'Irms':
          $seriesTitle = 'n';
          $seriesTitleValue = number_format($result_data[$seriesTitle][0][$serieskey],1);
          break;
      }
      $series[$serieskey] = array(
        'title' => str_replace('%value', $seriesTitleValue, ntua_wind_chart_series_title($seriesTitle)),
        'data' => $chart_data,
        'showInLegend' => !in_array($chart_type, $noSName),
        'showPoints' => (count($chart_data) < 20),
        'ttFormat' => "{point.x:.2f} ".str_replace('%value', '', ntua_wind_chart_series_title($xAxis)).
          "<br>{point.y:.2f} ".str_replace('%value', '', ntua_wind_chart_series_title($yAxis)),
      );
    }
  } else {
    $yAxis = $axis[0];
    $series[0] = array(
      'title' => t(ntua_wind_chart_axis_title($yAxis).$serieskey),
      'data' => $result_data[$yAxis],
      'axis' => $axis,
      'showInLegend' => !in_array($chart_type, $noSName),
      'ttFormat' => "{point.y:.2f} ".str_replace('%value', '', ntua_wind_chart_series_title($seriesTitle)),
    );
    switch ($yAxis) {
      case 'Bmaxfile':
        $xAxis = 'Bmaxfile_X';
        break;
      case 'Bmgfile':
        $xAxis = 'Bmgfile_X';
        break;;
    }
  }



  $response = array(
    // 'chart_type' => $chart_type,
    'chart_title' => ntua_wind_get_chart_list($chart_type),
    'series' => $series,
    'xAxis' => array(
      'title' => ntua_wind_chart_axis_title($xAxis),
    ),
    'yAxis' => array(
      'title' => ntua_wind_chart_axis_title($yAxis),
    ),
  );
  return $response;
}
