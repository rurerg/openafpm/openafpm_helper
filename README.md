# OpenAFPM Drupal helper module

This is a Drupal 7 module for serving and displaying OpenAFPM simulation data  
It server as a data exhange point for [OpenAFPM Worker](https://gitlab.com/openafpm/openafpm_worker/-/blob/master/README.md)

# License

GNU General Public License v3.0